
select case when buy  = 1 then 'BUY' when buy = 0 then 'SELL' end, size, price
from trade 
  join position  on position.opening_trade_id = trade.id or position.closing_trade_id = trade.id
where position.trader_id = 1 and trade.stock = 'MRK'  


--Find the total profit or loss for a given trader over the day, as the sum of the product of trade size and price for all sales,
-- minus the sum of the product of size and price for all buys.
 

 @profit = 0.0 ; 
set profit = ( sum (price * size ) from trade where buy = 0) 

